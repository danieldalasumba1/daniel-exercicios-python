

$`F_{bridge} = 2 \pi r_2 \gamma+ \pi r_2^2 \Delta P`$

$`\Delta P = \gamma \left(\frac{1}{r_1} -\frac{1}{r_2}\right)`$

Caso 1

Para 
$`\gamma=0,5 `$

$`r_1=1 m`$

$`r_2=1,5 m`$

$`r_2 > r_1`$

Teremos: 

$`\Delta P = 0,5(1/0,5-1/1,5)=0,4atm `$

Então:

$`F_{bridge} =  2*3,14*1,5*0,5+3,14*1,5.1,5*0,38= 7,4 N `$

Caso 2

Para 

$`\gamma =0,6`$

$`r_1 = 1,2 m`$

$`r_2 = 1,6 m`$

$`r_2 > r_1`$

Teremos:

$`\Delta P = 0,6(1/1,2-1/1,6) = 0,1atm`$


Então:


$`F_{bridge} = 2*3,14*1,6*0,6 + 3,14*1,6*1,6*0,1 = 7,1 N`$

Caso 3

$`\gamma = 1`$

$`r_1 = 1,3 m`$

$ `r_2 = 1,7 m `$

$`r_2 > r_1`$

Teremos:

$`\Delta P = 0,7(1/1,3-1/1,7) = 0,2 `$

$`F_{bridge} = 2*3,14*1*1,7 + 3,14*1,7*1,7*0,2 = 12,5 N`$


Caso 4

$`\gamma = 0,1`$

$`r_1 = 0,9 m`$

$`r_2 = 0,8 m`$

$`r_1 > r_2`$

Teremos:

$`\Delta P = 0,1(1/0,9-1/0,8) = -0,01 atm`$

Pressão obtida é monométrica(negativa) significa que a pressão é mais baixa que o ambiente.

Teremos $\gamma`$ negativo

E

$\F_{bridge} = 2*3,14*0,1*0,8 +3,14*0,8*0,8*(-0,01) = 0,48 N `$

Caso 5 

$`\gamma = 0,9`$

$`r_1 = 2 m`$

$`r_2 = 1 m`$

$` r_1 > r_2`$

Teremos:

$`\Delta P = 0,9(1/2-1/1) = -0,45atm`$


$`\F_{bridge} = 2*3,14*1*0,9 + 3,14*1*1*(-0,45) = 4,2 N`$

Caso 6

$`\gamma = 4`$

$`r_1 = 3`$

$`r_2 = 2`$

$`r_1 > r_2`$

$`\Delta P = 4(1/3-1/2) = -0,7`$

$ `F_{bridge} = 2*3,14*2*4+3,14*2*2*(-0,7) = 41,5 N`$












